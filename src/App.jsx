import NavBar from './components/NavBar'
import './App.css'
import { BrowserRouter,Route,Routes, useParams } from 'react-router-dom'
import { useEffect, useState } from 'react'
import SingleCountry from './components/SingleCountry'
import { Link } from 'react-router-dom'
import Error from './components/Error'
import MainContainer from "./components/MainContainer"


function App() {

  const [countryWantToFind, setCountryWantToFind] = useState("")
  
  function handleCountryUrl(countryName)
  {
    setCountryWantToFind(countryName);
  }
  
  return (
    <BrowserRouter>
      <NavBar />
      <Routes>

        <Route path="/" element={<MainContainer handleCountryUrl={handleCountryUrl}/>} /> 
        {/* <Route path="/:countryToFind" element={<SingleCountry countryWantToFind={ countryWantToFind} />} /> */}
        <Route path="/:countryCapital" element={ <SingleCountry />} />
        <Route path="*" element={ <Error />} />
  
      </Routes>
    
    </BrowserRouter>
    

  )
}

export default App

