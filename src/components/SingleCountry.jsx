import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import { useEffect } from 'react'
import { useParams } from 'react-router-dom'
import CountryLanguage from './CountryLanguage'

function BorderArray({countryData})
{
    let BorderCountries = countryData[0].borders.map(border => border)
    return BorderCountries.toString()
}


const SingleCountry = ({ }) => {
    const [countryData, setCountryData] = useState("")
    let { countryCapital } = useParams();
    
    useEffect(() => {
        fetch("https://restcountries.com/v3.1/capital/" + countryCapital)
            .then((response) => {
                return response.json()
            })
            .then((data) => {
                setCountryData(data)
            })
    }, [])

        return (
            <>
                <div>
                    <Link to="/" ><button>Back</button></Link>
                </div>
                <div className='m-5 d-flex' >
                    <img src={countryData ? countryData[0].flags?.png : ""} style={{ width:"30%" }} />
                    <div className='d-flex'>
                    <div >
                        <h1 className='m-5'>{countryData ? countryData[0].name.common : ""}</h1>
                        <ul>
                            {countryData ?
                                <>
                                    <h4>Native Name : {countryData[0].name.nativeName?.[Object.keys(countryData[0].name.nativeName)[0]].common || 'N/A'}</h4>
                                    <h4>Population :{countryData[0].population}</h4>
                                    <h4>Region:{countryData[0].region}</h4>
                                    <h4>Sub Region:{countryData[0].subregion}</h4>
                                    <h4>Capital:{countryData[0].capital}</h4>
                                </> : ""
                            }
                        </ul>
                    </div>
                    <div className='m-5 '>
                        {countryData ?
                            <div className='m-5 p-5' >
                                <h4>Total Level domain: be</h4>
                                <h4>Currencies: {Object.values(countryData[0].currencies)?.[0]?.name || "NA"}</h4>
                                <h4 >Languages:<CountryLanguage languageData={countryData[0].languages} /></h4>
                                </div> : ""
                                
                            }
                            
                        </div>
                        
                    </div>

                </div>
                <div className='d-flex'>
                    <h4>Border Countries: </h4>
                    {countryData ? countryData[0].borders?.length ? < BorderArray countryData={countryData} /> : <div>NA</div>  : ""}
                </div>


            </>
    )
}

export default SingleCountry