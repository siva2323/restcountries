const Sort = ({propFunction}) => {
    return (
        <div >
            <select onChange={propFunction}>
                <option value="">Sort by</option>
                <option value="Name">Name</option>
                <option value="Area">Area</option>
                <option value="Population">Population</option>
            </select>
        </div>
    )
}

export default Sort