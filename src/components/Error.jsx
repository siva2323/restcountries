import React from 'react'
import { Link } from 'react-router-dom'

const Error = () => {
  return (
      <div>
          <p>Error</p>
          <p>404</p>
          <p>Page not found</p>
      <Link to="/"><button className='btn btn-primary'>Go back to home</button></Link>
    </div>
  )
}

export default Error