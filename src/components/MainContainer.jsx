import React, { useEffect, useState } from 'react'
import getCountries from '../services/countryAPI';
import CountryList from './CountryList';
import Filter from './Filter';
import Search from './Search';
import Sort from './Sort';
import { Link } from 'react-router-dom';


const MainContainer = ({handleCountryUrl}) => {

  const [countries, setCountries] = useState([]) 
  const [region, setRegion] = useState("")
  const [subRegion, setSubRegion] = useState("")
  const [query, setQuery] = useState("")
  const [sortBy,setSortBy] =useState("")

  useEffect(() => {
    getCountries().then(countriesData => {
      setCountries(countriesData)
    })
  }, [])

  

  const countriesBySearch = query ? countries.filter(country => (((country.name.common).toLowerCase()).includes(query.toLowerCase()))) : countries

  function handleChangeSearch(event) {
    setQuery(event.target.value)
  }

  const regions = [...new Set(countries.map(country => country.region))]
  const filteredCountriesByRegion = region ? countriesBySearch.filter(country => country.region === region) : [];

  function handleChangeRegion(event) {
    setRegion(event.target.value);
    setSubRegion("")
  }
  
  const subRegions = [...new Set(filteredCountriesByRegion.map(country => country.subregion))]
  const filteredCountriesBySubRegion=region?(subRegion?filteredCountriesByRegion.filter(country => country.subregion === subRegion):filteredCountriesByRegion):countriesBySearch
  
  function handleChangeSubRegion(event) {
  setSubRegion(event.target.value)
  }

  const SortingName = (sortBy === "Name"? (filteredCountriesBySubRegion.sort((a, b) => {
    if (a.name.common > b.name.common) return 1;
    else return -1
  })) : filteredCountriesBySubRegion)
  
  const SortingPopulation = (sortBy === "Population" ? (SortingName.sort((a, b) => a.population - b.population)) : SortingName)
  const SortingArea = (sortBy === "Area" ? (SortingPopulation.sort((a, b) => a.area - b.area)) : SortingPopulation)



  function handleSorting(event) {
    setSortBy(event.target.value)
  }



  return (
    <>
    <div className="d-flex flex-wrap ">
      <Search propFunction={handleChangeSearch} />
      <Filter searchTerm={regions} propFunction={handleChangeRegion} filterBy="Filter by Region" />
      <Filter searchTerm={subRegions} propFunction={handleChangeSubRegion} filterBy="Filter by Subregion" />
        <Sort propFunction={handleSorting} />
      </div>
      <div className="d-flex flex-wrap justify-content between">
        <CountryList countries={SortingArea} countryNameForUrl={ handleCountryUrl} />
      </div>

      <h1 className='m-5'>{SortingArea.length < 1 && "Country not found"}</h1>

    </>
  )
}

export default MainContainer