import React from 'react'
import { Link } from 'react-router-dom'
const CountryList = ({ countries, countryNameForUrl }) => {
  return (
      <>
          {countries.map(country => {
              return (
                  <div key={country.flags?.png}>
                      <div className="card m-5" style={{ width: "18rem" }} onClick={ () => countryNameForUrl(country.capital)}>
                      <Link to={`${country.capital}`} >
                          <img src={country.flags?.png} className="card-img-top" alt="image" />
                          <div className="card-body">
                              <h5 className="card-title ">Country Name : { country.name.common}</h5>
                              <ul >
                                  <p>Capital : {country.capital}</p>
                                  <p>Region : {country.region}</p>
                                  <p>SubRegion : { country.subregion}</p>
                                  <p>Population : {country.population}</p>
                                  <p>Area : { country.area}</p>
                             </ul>
                             
                          </div>
                      </Link>
                      </div>
                </div>   
              )
          })}
      </>
  )
}

export default CountryList