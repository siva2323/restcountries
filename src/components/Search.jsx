import React from 'react'

const Search = ({propFunction}) => {
  return (
      <div>
          <input type="text" placeholder='Search country by name' onChange={ propFunction} />
    </div>
  )
}

export default Search