const Filter = ({ searchTerm,propFunction,filterBy}) => {
  
  return (
    <div >
      <select onChange={propFunction}>
        <option value="">{ filterBy}</option>
        {searchTerm.map((searchByValue,index) => <option value={searchByValue} key ={index+searchByValue}>{ searchByValue}</option>)}
      </select>
    </div>
  )
}

export default Filter